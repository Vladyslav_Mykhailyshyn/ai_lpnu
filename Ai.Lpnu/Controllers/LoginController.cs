﻿using Ai.Lpnu.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Raven.Client.Documents.Session;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ai.Lpnu.Controllers
{
    public class LoginController: BaseController
    {
        private readonly SignInManager<AppUser> signInManager;
        private readonly UserManager<AppUser> userManager;

        public LoginController(
            IAsyncDocumentSession dbSession, // injected thanks to Startup.cs call to services.AddRavenDbAsyncSession()
            UserManager<AppUser> userManager, // injected thanks to Startup.cs call to services.AddRavenDbIdentity<AppUser>()
            SignInManager<AppUser> signInManager) // injected thanks to Startup.cs call to services.AddRavenDbIdentity<AppUser>()
            : base(dbSession)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SignIn([FromForm]SignInModel model)
        {
            string reason = string.Empty;
            try
            {
                var user = (await DbSession.Advanced.LoadStartingWithAsync<AppUser>("AppUsers")).First();
                if (user.UserName == model.Login)
                {
                    var signInResult = await this.signInManager.PasswordSignInAsync(user.Email, model.Password, true, false);
                    if (signInResult.Succeeded)
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    reason = signInResult.IsLockedOut ? "Your user is locked out" :
                    signInResult.IsNotAllowed ? "Your user is not allowed to sign in" :
                    signInResult.RequiresTwoFactor ? "2FA is required" : "";
                }
                else
                {
                    reason = "Bad user name or password";
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return RedirectToAction("SignInFailure", new { reason });
        }

        [HttpGet]
        public IActionResult SignInFailure(string reason)
        {
            ViewBag.FailureReason = reason;
            return View();
        }
      

        [HttpGet]
        public IActionResult ChangeRoles()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register([FromBody]RegisterModel model)
        {
            // Create the user.
            var appUser = new AppUser
            {
                Email = model.Email,
                UserName = model.Login
            };
            try
            {
                var createUserResult = await this.userManager.CreateAsync(appUser, model.Password);
                if (!createUserResult.Succeeded)
                {
                    var errorString = string.Join(", ", createUserResult.Errors.Select(e => e.Description));
                    return RedirectToAction("RegisterFailure", new { reason = errorString });
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }


            // Sign him in and go home.
            await this.signInManager.SignInAsync(appUser, true);
            return this.RedirectToAction("Index", "Admin");
        }

        [HttpGet]
        public async Task<IActionResult> SignOut()
        {
            await this.signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}
