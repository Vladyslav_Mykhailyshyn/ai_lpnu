﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ai.Lpnu.Models
{
    public class AppUser : Raven.Identity.IdentityUser
    {
        /// <summary>
        /// A user's full name.
        /// </summary>
        public string FullName { get; set; }
    }
}
