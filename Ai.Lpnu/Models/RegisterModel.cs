﻿
namespace Ai.Lpnu.Models
{
    public class RegisterModel
    {
        public string Email { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
