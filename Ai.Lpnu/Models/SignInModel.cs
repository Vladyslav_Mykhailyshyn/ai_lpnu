﻿
namespace Ai.Lpnu.Models
{
    public class SignInModel
    {
        public string Login { get; set; }
        public string Password { get; set; }
    }
}
